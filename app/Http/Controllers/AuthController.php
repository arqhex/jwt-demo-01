<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterAuthRequest;
use App\User;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller {
    
    public $loginAfterSignUp = true;

    public function register(Request $request) {

        if ($request->isMethod('post') && isset($request->login) && !empty($request->login) && isset($request->password) && !empty($request->password)){

            if(!$this->existe($request->login, $request->password)){
                $user = new  User();
                $user->nombre = $request->nombre;
                $user->apellidos = $request->apellidos;
                $user->login = $request->login;
                $user->password = bcrypt($request->password);                
                $user->save();

                if ($this->loginAfterSignUp) {
                    return  $this->login($request);
                }

                return  response()->json([
                    'status' => 'ok',
                    'data' => $user
                ], 200);    

            } else {
                return  response()->json([
                    'status' => 'ok',
                    'data' => [],
                    'message' => 'El usuario ya existe'
                ], 200);                    
            }
        }

    }

    private function existe($login, $password) {
        $rs = DB::table('usuario')->select('password')->where(['login' => $login]);
        $password_guardada = $rs->first();
        
        $conicide_password = false;
        if( $password_guardada != null && isset($password_guardada->password) ) {
            $conicide_password = Hash::check($password, $password_guardada->password);
        }

        return $conicide_password;
    }    

    public function login(Request $request) {
        if( isset($request->login) && !empty($request->login) && isset($request->password) && !empty($request->password) ){
            $input = $request->only('login', 'password');
            $jwt_token = null;
            if( !$jwt_token = JWTAuth::attempt($input)  ) {
                return response()->json([ 
                    'status' => 0,
                    'message' => 'Login y/o password invalidos',
                    'data' => ['token' => null]
                ], 401);
            }
            return response()->json([ 
                'status' => 1, 
                'data' => [
                    'token' => $jwt_token
                ], 
                'message' => 'Autenticacion OK'
            ], 200);
        } else {
            return response()->json([ 
                'status' => 0, 
                'data' => [
                    'token' => null
                ], 
                'message' => '(Login, password) parameters missing'
            ], 200);
        }
    }

    public function logout(Request $request) {
        $this->validate($request, [
			'token' => 'required'
		]);

		try {
			JWTAuth::invalidate($request->token);
			return  response()->json([
				'status' => 'ok',
				'message' => 'Cierre de sesion exitoso'
			]);
		} catch (JWTException  $exception) {
			return  response()->json([
				'status' => 'unknown_error',
				'message' => 'Al usuario no se le pudo cerrar la sesion'
			], 500);
		}
    }

    public function getAuthUser(Request $request) {
        $this->validate($request, [
			'token' => 'required'
		]);
		$user = JWTAuth::authenticate($request->token);
		return  response()->json(['user' => $user]);
    }
}
