<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class PruebaController extends Controller {
    
    public function index(){

        $login = 'ldanielbg@comunidad.unam.mxyz';
        $password = 'hola123.';
                
        return view('prueba.index', [
            'data' => [                
                'resultSet' => $this->existe($login, $password)
            ] 
        ]);

    }

    private function existe($login, $password) {
        $result_set = DB::table('usuario')->select('password')->where(['login' => $login]);
        $password_guardado = $result_set->first();
        
        $conicide_password = false;
        $password_encontrado = ($password_guardado != null) && (isset($password_guardado->password));
        if ( $password_encontrado ) {
            $conicide_password = Hash::check($password, $password_guardado->password);
        }
        return $conicide_password;
    }
}
