# JWT Demo 01
Primer demo usando Laravel y JWT para la generación de access-tokens.
## Requerimientos
Las tecnologías y versiones con las que fue hecho este demo:
- PHP 7.4
- Composer 1.9.3
- Laravel 6
- JWT
- Manejador PostgreSQL 11 para la base de datos
- FlywayDB 6.4.3 para las migraciones de esquema de base de datos

## Instalación del demo
1. Clona el repositorio
2. Ejecuta composer install
3. Crea la basde de datos de la aplicación. Sin tablas, solo la base de datos.

## Instalación de la base de datos
1. Descarga el paquete FlywayDB 6.4.3.
2. Descomprime el paquete y copia su contenido sobre el directorio de flyway-6.4.3 la raíz del proyecto sin eliminar los scripts SQL que se encuentran en flyway-6.4.3/sql.
3. Edita el archivo flyway-6.4.3/conf/flyway.conf con los parámetros de conexión correctos de la base de datos creada para esta aplicación.
4. Ubicate en el directorio flyway-6.4.3 desde la línea de comandos y ejecuta el siguiente comando para crear los objetos de la base de datos.
```sh
flyway migrate
```

## Cómo se hizo
```sh
composer create-project laravel/laravel api-gateway-demo-01 6.*
cd api-gateway-demo-01
composer require tymon/jwt-auth
php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"
git init .
git add -A
git commit -m "Commit inicial"
php artisan jwt:secret
```