-- Script sql con la creacion de la BD
-- Autor: Daniel Barajas Gonzalez
-- Fecha: 05 de junio de 2020

drop table if exists usuario ;

create table usuario (
    id serial primary key,
    nombre varchar(80) not null,
    apellidos varchar(120) null,
    login varchar(255) unique,
    password varchar(255) not null,
    borrado boolean default false,

    fecha_hora_creacion timestamp without time zone default now(),
    fecha_hora_actualizacion timestamp without time zone default null,
    fecha_hora_borrado timestamp without time zone default null
);

