-- Se crea el trigger para actualizar el campo de fecha y hora
-- cuando ocurra una actualizacion directo en la base de datos
-- Autor: Daniel Barajas Gonzalez
-- Fecha: 05 de junio 2020

Create or replace function registro_actualizado() 
Returns trigger as $BODY$
BEGIN
    new.fecha_hora_actualizacion = now();
    return new;
END;
$BODY$ Language plpgsql Volatile Cost 100;

Create trigger trg_actualiza_usuario
Before update on usuario
For each row execute procedure registro_actualizado();